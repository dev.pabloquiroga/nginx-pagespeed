#!/bin/bash
docker build . -t nginx-pagespeed:latest
docker image prune --filter label=stage=builder -f
docker run -v $PWD:/opt/mount --rm --entrypoint cp nginx-pagespeed:latest /root/ngx_pagespeed.so /opt/mount/ngx_pagespeed.so