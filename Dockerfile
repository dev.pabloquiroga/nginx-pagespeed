FROM ubuntu:latest AS builder

ENV NGINX_VERSION=1.21.3
ENV NPS_VERSION=1.13.35.2-stable 

LABEL stage=builder

RUN ln -fs /usr/share/zoneinfo/America/Buenos_Aires /etc/localtime

RUN apt-get update && \
    apt-get install gnupg -y

RUN echo "deb http://ppa.launchpad.net/ondrej/nginx-mainline/ubuntu focal main" >> /etc/apt/sources.list && \
    echo "deb-src http://ppa.launchpad.net/ondrej/nginx-mainline/ubuntu focal main" >> /etc/apt/sources.list && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 14AA40EC0831756756D7F66C4F4EA0AAE5267A6C

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    git \
    wget \
    nginx \
    libxslt-dev \
    libgd-dev \
    build-essential \
    zlib1g-dev \
    libpcre3-dev \
    unzip \
    uuid-dev \
    libssl-dev

COPY ./compile.sh /root/compile.sh
RUN chmod a+x /root/compile.sh && /root/compile.sh

FROM nginx:1.21.3-alpine AS run 

COPY --from=builder /root/ngx_pagespeed.so /root/ngx_pagespeed.so